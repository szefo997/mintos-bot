module.exports = {
  name: 'mintos-bot',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/mintos-bot',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
