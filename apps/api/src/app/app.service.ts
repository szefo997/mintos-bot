import { Injectable } from '@nestjs/common';
import * as puppeteer from 'puppeteer';


@Injectable()
export class AppService {

  public async getPuppeteerBrowser(): Promise<any> {
    return await puppeteer.launch({
      headless: true,
      slowMo: 250,
      // 60s
      timeout: 60000,
      defaultViewport: {
        width: 1920,
        height: 1080
      }
    });
  }

}
