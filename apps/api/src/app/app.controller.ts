import { Controller, Get } from '@nestjs/common';

@Controller()
export class AppController {
  constructor() {
  }

  @Get()
  findAll() {
    return 'The app is running.';
  }

}
