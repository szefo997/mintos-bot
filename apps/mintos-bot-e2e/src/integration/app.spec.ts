import { getGreeting } from '../support/app.po';

describe('mintos-bot', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to mintos-bot!');
  });
});
